from django.contrib import admin
from .models import *

admin.site.register(Samples)
admin.site.register(Tissue)
admin.site.register(PatientInfo)
admin.site.register(Biobank)
admin.site.register(CultureInfo)
admin.site.register(Mutations)
admin.site.register(MolecularOrder)
admin.site.register(Processing)
admin.site.register(Freezing)


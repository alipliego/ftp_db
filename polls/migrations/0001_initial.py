# Generated by Django 2.1.15 on 2021-03-04 00:15

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Biobank',
            fields=[
                ('BiobankID', models.CharField(max_length=20, primary_key=True, serialize=False)),
            ],
        ),
        migrations.CreateModel(
            name='CultureInfo',
            fields=[
                ('SubtypeID', models.CharField(max_length=20, primary_key=True, serialize=False)),
                ('SubtypeInfo', models.CharField(max_length=30, null=True)),
                ('FirstCulture', models.DateTimeField(blank=True, null=True, verbose_name='First Passage')),
                ('CultureStatus', models.CharField(blank=True, max_length=40, null=True)),
                ('Passage', models.CharField(blank=True, max_length=30, null=True)),
                ('DefrozePassage', models.CharField(blank=True, max_length=40, null=True)),
                ('Media', models.CharField(blank=True, max_length=30, null=True)),
                ('Type', models.CharField(blank=True, max_length=40, null=True)),
                ('CultureType', models.CharField(blank=True, max_length=30, null=True)),
                ('Status', models.CharField(blank=True, max_length=40, null=True)),
                ('LastCulture', models.DateTimeField(blank=True, null=True, verbose_name='Last Passage')),
                ('CellBlock', models.CharField(blank=True, max_length=30, null=True)),
                ('KnownMutation', models.CharField(blank=True, max_length=30, null=True)),
                ('MolecularOrder', models.CharField(blank=True, max_length=10, null=True)),
                ('PatientConsent', models.CharField(blank=True, max_length=30, null=True)),
                ('DateOfConsent', models.DateTimeField(blank=True, null=True, verbose_name='Date of Consent')),
                ('Note', models.CharField(blank=True, max_length=120, null=True)),
                ('Note2', models.CharField(blank=True, max_length=120, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Freezing',
            fields=[
                ('FreezingID', models.CharField(max_length=20, primary_key=True, serialize=False)),
                ('Amount', models.IntegerField(null=True)),
                ('FreezingType', models.CharField(max_length=20, null=True)),
                ('Location', models.CharField(max_length=20, null=True)),
                ('Tower', models.CharField(max_length=20, null=True)),
                ('Box', models.IntegerField(null=True)),
                ('Start', models.DateField(null=True, verbose_name='Date of Start')),
                ('End', models.DateField(null=True, verbose_name='Date of End')),
                ('Comments', models.TextField(blank=True)),
                ('FrozenBy', models.CharField(max_length=20, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='MolecularOrder',
            fields=[
                ('MolecularOrderID', models.CharField(max_length=20, primary_key=True, serialize=False)),
                ('MolecularOrderNumber', models.CharField(max_length=30, null=True)),
                ('AssayType', models.CharField(max_length=30, null=True)),
                ('SubtypeID', models.ForeignKey(max_length=20, on_delete=django.db.models.deletion.CASCADE, to='polls.CultureInfo')),
            ],
        ),
        migrations.CreateModel(
            name='Mutations',
            fields=[
                ('MutationID', models.CharField(max_length=20, primary_key=True, serialize=False)),
                ('GeneName', models.CharField(max_length=30, null=True)),
                ('Chromosome', models.CharField(max_length=10, null=True)),
                ('ChromosomePosition', models.IntegerField(null=True)),
                ('GenePosition', models.IntegerField(null=True)),
                ('ProteinPosition', models.IntegerField(null=True)),
                ('SubtypeID', models.ForeignKey(max_length=20, on_delete=django.db.models.deletion.CASCADE, to='polls.CultureInfo')),
            ],
        ),
        migrations.CreateModel(
            name='PatientInfo',
            fields=[
                ('PatientID', models.CharField(max_length=20, primary_key=True, serialize=False)),
                ('Name', models.CharField(max_length=30, null=True)),
                ('Surname', models.CharField(max_length=40, null=True)),
                ('Gender', models.CharField(max_length=20, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Processing',
            fields=[
                ('ProcessingID', models.CharField(max_length=20, primary_key=True, serialize=False)),
                ('TumorType', models.CharField(blank=True, max_length=40, null=True)),
                ('PassageDate', models.DateTimeField(blank=True, null=True)),
                ('Media', models.CharField(blank=True, max_length=50, null=True)),
                ('PassageNumber', models.IntegerField(blank=True, null=True)),
                ('DefrozePassage', models.IntegerField(blank=True, null=True)),
                ('Status', models.CharField(blank=True, max_length=40, null=True)),
                ('Format', models.CharField(blank=True, max_length=40, null=True)),
                ('CultureType', models.CharField(max_length=30, null=True)),
                ('FormatNumber', models.CharField(max_length=30, null=True)),
                ('CellBlock', models.CharField(max_length=30, null=True)),
                ('Smear', models.CharField(max_length=30, null=True)),
                ('DateBlock', models.DateTimeField(blank=True, null=True)),
                ('Comments', models.TextField(blank=True, null=True)),
                ('SubtypeID', models.ForeignKey(max_length=20, on_delete=django.db.models.deletion.CASCADE, to='polls.CultureInfo')),
            ],
        ),
        migrations.CreateModel(
            name='Samples',
            fields=[
                ('SampleID', models.CharField(max_length=20, primary_key=True, serialize=False)),
                ('SampleName', models.CharField(blank=True, max_length=50, null=True)),
                ('Organ', models.CharField(max_length=40, null=True)),
                ('TumorType', models.CharField(max_length=50, null=True)),
                ('Additional', models.CharField(max_length=40, null=True)),
                ('MolecularGeneral', models.CharField(max_length=40, null=True)),
                ('MolecularOrganoid', models.CharField(max_length=40, null=True)),
                ('GeneralConsent', models.CharField(max_length=40, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Tissue',
            fields=[
                ('TissueID', models.CharField(max_length=20, primary_key=True, serialize=False, verbose_name='TissueID')),
                ('SampleID', models.ForeignKey(max_length=20, on_delete=django.db.models.deletion.CASCADE, to='polls.Samples')),
            ],
        ),
        migrations.AddField(
            model_name='patientinfo',
            name='TissueID',
            field=models.ForeignKey(max_length=20, on_delete=django.db.models.deletion.CASCADE, to='polls.Tissue'),
        ),
        migrations.AddField(
            model_name='freezing',
            name='ProcessingID',
            field=models.ForeignKey(max_length=20, on_delete=django.db.models.deletion.CASCADE, to='polls.Processing'),
        ),
        migrations.AddField(
            model_name='cultureinfo',
            name='SampleID',
            field=models.ForeignKey(max_length=20, on_delete=django.db.models.deletion.CASCADE, to='polls.Samples'),
        ),
        migrations.AddField(
            model_name='biobank',
            name='TissueID',
            field=models.ForeignKey(max_length=20, on_delete=django.db.models.deletion.CASCADE, to='polls.Tissue'),
        ),
    ]

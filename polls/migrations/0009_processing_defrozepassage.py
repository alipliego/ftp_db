# Generated by Django 2.1.15 on 2021-03-04 01:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0008_auto_20210304_0106'),
    ]

    operations = [
        migrations.AddField(
            model_name='processing',
            name='DefrozePassage',
            field=models.IntegerField(blank=True, null=True),
        ),
    ]

#root /path/to/models.py
#models package from Django.db

import django 
from django.db import models 

class Samples(models.Model):
    SampleID = models.CharField(primary_key = True, max_length = 20)
    SampleName = models.CharField(max_length = 20, null=True)
    CancerType = models.CharField(max_length = 30, null= True)
    def __str__(self):
        return self.SampleID

class Tissue(models.Model):
    TissueID = models.CharField('TissueID', primary_key = True, max_length = 20)
    SampleID = models.ForeignKey(Samples, max_length = 20, on_delete = models.CASCADE)
    def __str__(self):
        return self.TissueID

class PatientInfo(models.Model):
    PatientID = models.CharField(primary_key = True, max_length = 20)
    TissueID  = models.ForeignKey(Tissue, max_length = 20, on_delete = models.CASCADE)
    Name      = models.CharField(max_length = 30, null=True)
    Surname   = models.CharField(max_length = 40, null= True)
    Gender    = models.CharField(max_length = 20, null=True)

class Biobank(models.Model):
    BiobankID = models.CharField(primary_key = True, max_length = 20)
    TissueID  = models.ForeignKey(Tissue, max_length = 20, on_delete = models.CASCADE)
    def __str__(self):
        return self.BiobankID        

class CultureInfo(models.Model):
    SubtypeID        = models.CharField(primary_key = True, max_length = 20)
    SampleID         = models.ForeignKey(Samples, max_length = 20, on_delete = models.CASCADE)
    SubtypeInfo      = models.CharField(max_length = 30)
    FirstCulture     = models.DateField(input_formats =  ['%d-%m-%Y']wq)
    Passage          = models.IntegerField()
    Media            = models.CharField(max_length = 30, null = True, blank=True)
    CultureType      = models.CharField(max_length = 30, null=True, blank=True)
    CellBlock        = models.CharField(max_length = 10, null= True, blank=True)
    KnownMutation    = models.CharField(max_length = 30, null= True, blank=True)
    MolecularOrder   = models.CharField(max_length = 10, null= True, blank=True)
    PatientConsent   = models.CharField(max_length = 30, null = True, blank=True)
    DateOfConsent    = models.DateField('Date of Consent', null = True, blank=True)
    Comments         = models.TextField(blank = True)
    def __str__(self):
        return self.SubtypeID

class Mutations(models.Model):
    MutationID         = models.CharField(primary_key = True, max_length = 20)
    SubtypeID          = models.ForeignKey(CultureInfo, max_length = 20, on_delete = models.CASCADE)
    GeneName           = models.CharField(max_length = 30, null = True)
    Chromosome         = models.CharField(max_length = 10, null = True)
    ChromosomePosition = models.IntegerField(null = True)
    GenePosition       = models.IntegerField(null = True)
    ProteinPosition    = models.IntegerField(null = True)

class MolecularOrder(models.Model):
    MolecularOrderID     = models.CharField(primary_key = True, max_length = 20)
    SubtypeID            = models.ForeignKey(CultureInfo, max_length = 20, on_delete = models.CASCADE)
    MolecularOrderNumber = models.CharField(max_length = 30, null = True)
    AssayType            = models.CharField(max_length = 30, null = True)

class Processing(models.Model):
    ProcessingID  = models.CharField(primary_key = True, max_length = 20)
    SubtypeID     = models.ForeignKey(CultureInfo, max_length = 20, on_delete = models.CASCADE)
    Subcategory   = models.CharField(max_length = 10, null = True)
    PassageDate   = models.DateField(null = True)
    Media         = models.CharField(max_length = 20, null = True)
    PassageNumber = models.IntegerField(null = True)
    Format	  = models.IntegerField(null = True)
    CultureType   = models.CharField(max_length = 20, null = True)
    Comments      = models.TextField(blank = True, null = True)


class Freezing(models.Model):
    FreezingID   = models.CharField(primary_key = True, max_length = 20)
    ProcessingID = models.ForeignKey(Processing, max_length = 20, on_delete = models.CASCADE)
    Amount       = models.IntegerField(null = True)
    FreezingType = models.CharField(max_length = 20, null = True)
    Location     = models.CharField(max_length = 20, null = True)
    Tower        = models.CharField(max_length = 20, null = True)
    Box          = models.IntegerField(null = True)
    Start        = models.DateField('Date of Start', null = True)
    End          = models.DateField('Date of End', null = True)
    Comments     = models.TextField(blank = True)
    FrozenBy     = models.CharField(max_length = 20, null = True)

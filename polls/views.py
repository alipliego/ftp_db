from django.shortcuts import render, redirect
from django.shortcuts import get_object_or_404
from django.http import HttpResponse
from django_tables2 import RequestConfig
from .models import *
from .tables import *
from .filters import *
from django.views.generic.edit import DeleteView
from urllib.parse import unquote_to_bytes
from Crypto.Cipher import AES
from Crypto.Util.Padding import pad
from Crypto.Util.Padding import unpad
from django.urls import reverse_lazy
from .forms import CultureInfoForm, SamplesForm, MolecularOrderForm, PassagesForm,  FreezingForm
import pandas as pd 
import xlrd



get = {'userName': '%D5%AD%97%D7%FCf%B6_%A1%91s%B8%84%A4%F0%3E'}

username_hash = unquote_to_bytes(get['userName'])

key = b'QRiyMu5gNZMrhQKdEN9aD33PfXamrjUK'
iv = b'1535947594751664'

cipher = AES.new(key, AES.MODE_CBC, iv)



def index(request):
    queryset = Samples.objects.all() #.filter(order_date = '2020-11-16')
    data = request.GET
    table_filter = SamplesFilter(data,queryset)
    table_name = Samples.__name__
    table = SamplesTable(table_filter.qs)
    paginate = {'per_page': 10, 'page': 1}
    RequestConfig(request,paginate).configure(table)
    template = "main/index.html"
    context = {'table': table, 'filter': table_filter, 'table_name': table_name}
    return render(request, template, context)

def editSamples(request, sample_id):
    sample = get_object_or_404(Samples, pk = sample_id)
    form = SamplesForm(request.POST or None, instance=sample)
    if form.is_valid():
        form.save()
        return redirect('index')
        # redirect or show save form again
    
    context = {'culture' : sample, 'form': form}
    template = "main/editSamples.html"
    return render(request, template, context)

def culturesView(request, sample_id):
    queryset = CultureInfo.objects.filter(SampleID__SampleID = sample_id)
    data = request.GET
    table_filter = CulturesFilter(data,queryset)
    table_name = CultureInfo.__name__
    sample_name = Samples.objects.get(pk = sample_id)
    table = CulturesTable(table_filter.qs)
    paginate = {'per_page': 10, 'page': 1}
    RequestConfig(request,paginate).configure(table)
    template = "main/culturesView.html"
    context = {'table': table, 'filter': table_filter, 'table_name': table_name, 'sample_name': sample_name}
    return render(request, template, context)


def culturesAll(request):
    queryset = CultureInfo.objects.all()
    data = request.GET
    table_filter = CulturesFilter(data,queryset)
    table_name = CultureInfo.__name__
    table = CulturesTable(queryset)
    paginate = {'per_page': 10, 'page': 1}
    RequestConfig(request,paginate).configure(table)
    template = "main/culturesView.html"
    context = {'table': table,  'filter': table_filter,'table_name': table_name}
    return render(request, template, context)


def editCulture(request, sample_id):
    culture = get_object_or_404(CultureInfo, pk = sample_id)
    form = CultureInfoForm(request.POST or None, instance=culture)
    if form.is_valid():
        return redirect('culturesView', sample_id=form.cleaned_data['SampleID'].SampleID)
        form.save()
        # redirect or show save form again
    
    context = {'culture' : culture, 'form': form}
    template = "main/editCulture.html"
    return render(request, template, context)

def addCulture(request):
    form = CultureInfoForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect('culturesView', sample_id=form.cleaned_data['SampleID'].SampleID)
        # redirect or show save form again
    
    context = {'form': form}
    template = "main/editCulture.html"
    return render(request, template, context)

def addSample(request):
    form = SamplesForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect('index',)
        # redirect or show save form again
    
    context = {'form': form}
    template = "main/samples-form.html"
    return render(request, template, context)


def molecularorder(request, subtype_id):
    queryset = MolecularOrder.objects.filter(SubtypeID = subtype_id)
    data = request.GET
    table_name = MolecularOrder.__name__
    culture_info_object = get_object_or_404(CultureInfo, pk = subtype_id)
    sample_name = culture_info_object.SampleID.SampleName
    table = MolecularOrderTable(MolecularOrder.objects.filter(SubtypeID=subtype_id))
    paginate = {'per_page': 10, 'page': 1}
    RequestConfig(request,paginate).configure(table)
    template = "main/molecularorder.html"
    context = {'table': table, 'table_name': table_name,'sample_name': sample_name}
    return render(request, template, context)

def addMolecularOrder(request):
    form = MolecularOrderForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect('molecularorder', subtype_id=form.cleaned_data['SubtypeID'].SubtypeID)
        # redirect or show save form again
        # redirect or show save form again
    
    context = {'form': form}
    template = "main/molecularorder-form.html"
    return render(request, template, context)

def editMolecularOrder(request, subtype_id):
    mo = get_object_or_404(MolecularOrder, pk = subtype_id)
    form = SamplesForm(request.POST or None, instance=mo)
    if form.is_valid():
        form.save()
        return redirect('molecularorder')
        # redirect or show save form again
    
    context = {'culture' : mo, 'form': form}
    template = "main/editSamples.html"
    return render(request, template, context)


def passages(request, subtype_id):
    queryset = Processing.objects.filter(SubtypeID = subtype_id)
    data = request.GET
    table_name = Processing.__name__
    culture_info_object = get_object_or_404(CultureInfo, pk = subtype_id)
    sample_name = culture_info_object.SampleID.SampleName
    table = ProcessingTable(queryset)
    paginate = {'per_page': 10, 'page': 1}
    RequestConfig(request,paginate).configure(table)
    template = "main/passages.html"
    context = {'table': table,  'table_name': table_name, 'sample_name': sample_name}
    return render(request, template, context)


def passagesAll(request):
    queryset = Processing.objects.all()
    data = request.GET
    table_filter = CulturesFilter(data,queryset)
    table_name = Processing.__name__
    table = ProcessingTable(queryset)
    paginate = {'per_page': 10, 'page': 1}
    RequestConfig(request,paginate).configure(table)
    template = "main/passages.html"
    context = {'table': table,'filter':table_filter,  'table_name': table_name}
    return render(request, template, context)

def addPassages(request):
    form = PassagesForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect('passages', subtype_id=form.cleaned_data['SubtypeID'].SubtypeID)
        # redirect or show save form again
        # redirect or show save form again
    
    context = {'form': form}
    template = "main/passages-form.html"
    return render(request, template, context)

def editPassages(request, subtype_id):
    passage = get_object_or_404(Processing, pk = subtype_id)
    form = PassagesForm(request.POST or None, instance=passage)
    if form.is_valid():
        form.save()
        return redirect('passages')
        # redirect or show save form again
    
    context = {'culture' : passage, 'form': form}
    template = "main/editPassages.html"
    return render(request, template, context)

def freezing(request, sample_id):
    queryset = Freezing.objects.filter(SampleID = sample_id)
    data = request.GET
    table_name = Freezing.__name__
    freezing_info_object = get_object_or_404(Samples, pk = sample_id)
    sample_name = freezing_info_object.SampleID
    table = FreezingTable(queryset)
    paginate = {'per_page': 10, 'page': 1}
    RequestConfig(request,paginate).configure(table)
    template = "main/freezing.html"
    context = {'table': table,  'table_name': table_name, 'sample_name': sample_name}
    return render(request, template, context)

def freezingAll(request):
    queryset = Freezing.objects.all()
    data = request.GET
    table_filter = FreezingFilter(data,queryset)
    table_name = Freezing.__name__
    table = FreezingTable(queryset)
    paginate = {'per_page': 10, 'page': 1}
    RequestConfig(request,paginate).configure(table)
    template = "main/freezing.html"
    context = {'table': table,'filter':table_filter,  'table_name': table_name}
    return render(request, template, context)

def addFreezing(request):
    form = FreezingForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect('freezing', processing_id=form.cleaned_data['FreezingID'].FreezingID)
        # redirect or show save form again
        # redirect or show save form again
    
    context = {'form': form}
    template = "main/freezing-form.html"
    return render(request, template, context)


def editFreezing(request, sample_id):
    freezing = get_object_or_404(Freezing, pk = sample_id)
    form = FreezingForm(request.POST or None, instance=freezing)
    if form.is_valid():
        form.save()
        return redirect('freezing')
        # redirect or show save form again
    
    context = {'culture' : freezing, 'form': form}
    template = "main/editFreezing.html"
    return render(request, template, context)

class CultureInfoDelete(DeleteView):
    model = CultureInfo
    success_url = reverse_lazy('index')

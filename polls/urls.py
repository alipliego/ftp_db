from django.urls import path
from django.urls import re_path 
from django.conf.urls import url
from . import views
from django.contrib import admin
from polls.views import *
from django.views.generic.base import TemplateView


urlpatterns = [

    path('freezingAll/', views.freezingAll,name = 'freezingAll'),
    path('', views.index, name='index'),
    path('culturesAll/', views.culturesAll, name = 'culturesAll'),
    path('<str:sample_id>/culturesView/', views.culturesView, name='culturesView'),
    path('addSample/', views.addSample, name='addSample'),
    path('passagesAll/', views.passagesAll, name='pasagesAll'),
    path('index', views.index, name='index'),
    path('<str:sample_id>/editSamples/', views.editSamples, name='editSamples'),
    path('<str:sample_id>/editCulture/', views.editCulture, name='editCulture'),
    path('<str:pk>/deleteCulture/', views.CultureInfoDelete.as_view(), name='deleteCulture'),
    path('addCulture/', views.addCulture, name='addCulture'),
    path('<str:subtype_id>/molecularorder/', views.molecularorder, name='molecularorder'),
    path('addMolecularOrder/', views.addMolecularOrder, name='addMolecularOrder'),
    path('<str:subtype_id>/editMolecularOrder/', views.editMolecularOrder, name='editMolecularOrder'),
    path('<str:subtype_id>/passages/', views.passages, name='passages'),
    path('addPassages/', views.addPassages, name='addPassages'),
    path('<str:subtype_id>/editPassages/', views.editPassages, name='editPassages'),
    path('<str:sample_id>/freezing/', views.freezing, name='freezing'),
    path('addFreezing/', views.addFreezing, name='addFreezing'),
    path('<str:sample_id>/editFreezing/', views.editFreezing, name='editFreezing'),
]




